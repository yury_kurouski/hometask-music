import './styles.css'

export const Footer = () => {
  const footer = document.createElement('footer');

  footer.id='footer';

  footer.innerHTML = `
    <ul class='footer_links container'>
      <li class='footer_link'>About Us</li>
      <li class='footer_link'>Contact</li>
      <li class='footer_link'>CR Info</li>
      <li class='footer_link'>Twitter</li>
      <li class='footer_link'>Facebook</li>
    </ul>
  `

  return footer;
}