import { navigateTo } from '../../router';
import { parseLocation } from '../../router/utils';
import './styles.css';

type THeaderItem = {
  [key: string]: string[]
}

const headerItems: THeaderItem = {
  '/': [
    'Discover',
    'Join',
    'Sign In'
  ],
  '/discover': [
    'Join',
    'Sign In'
  ],
  '/join': [
    'Discover',
    'Sign In'
  ],
  '/sign': [
    'Discover',
    'Sign In'
  ]
}

export let testVar = '/'

export const Header = () => {
  const currentPath = parseLocation();

  const header = document.createElement('header');
  header.id = 'header';

  header.innerHTML = `
    <nav class='container'>
      <ul class='header'>
        <li class='header_element header_logo'>
          <a href='/' class='header-link' data-path='/'>Simo</a>
        </li>

        ${headerItems[currentPath].map(el => `
          <li class='header_element'>
            <a href='/${el}' class='header-link' data-path=${el}>${el}</a>
           </li>
        `).join('')}

      </ul>
    </nav>
  `

  header.addEventListener('click', (e) => {
    e.preventDefault()

    const target = e.target as HTMLElement

    if (target?.className === 'header-link' && target.dataset?.path) {
      const app = document.querySelector('#app')!;
      
      const newPath = target.dataset?.path.replace(/\s/g, '').toLowerCase();

      app.className = newPath;

      navigateTo(newPath);
    }
  });

  return header;
}