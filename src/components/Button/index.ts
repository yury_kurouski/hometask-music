import './styles.css'

export const Button = (text: string) => {


  return `
    <button class='main-button'>
      ${text}
    </button>
  `
}