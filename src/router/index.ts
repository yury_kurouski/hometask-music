import { Footer } from "../components/Footer";
import { Header } from "../components/Header";
import { DiscoverPage } from "../pages/DiscoverPage/DiscoverPage";
import { MainPage } from "../pages/MainPage";
import { SignUpPage } from "../pages/SignUpPage/SignUpPage";
import { parseLocation } from "./utils";

type TRoutes = {
  [key: string]: () => string
}
const routes: TRoutes = {
  '/discover': DiscoverPage,
  '/join': SignUpPage,
  '/sign': SignUpPage,
}

export const renderPage = () => {
  const pathname = parseLocation()

  const page = routes[pathname] || MainPage;

  document.getElementById('app')!.innerHTML = page();

  document.getElementById('app')!.prepend(Header());
  document.getElementById('app')!.appendChild(Footer());
}

export const navigateTo = (path: string) => {
  window.history.pushState({}, path, path);
  renderPage();
}
