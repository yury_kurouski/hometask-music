import './styles.css'

export const SignUpPage = () => {

  return `
    <main class='container signup'>
      <form class='signup_form'>
        <div class='signup_input-container'>
          <label for="username" class='signup_input-label'>Name:</label>
          <input id="username" name="username" type="text" class='signup_form_input'/> 
        </div>
        <div class='signup_input-container'>
          <label for="password" class='signup_input-label'>Password:</label>
          <input id="password" name="password" type="text" class='signup_form_input'/> 
        </div>
        <div class='signup_input-container'>
          <label for="e-mail" class='signup_input-label'>e-mail:</label>
          <input id="e-mail" name="e-mail:" type="text" class='signup_form_input'/> 
        </div>
      </form>
    </main>
  
  `
}