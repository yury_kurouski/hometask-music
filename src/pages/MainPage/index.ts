import { Button } from '../../components/Button'
import './styles.css'

export const MainPage = () => {
  return `
    <main class='container main'>
      <section class='main_content'>
        <h1 class='content_heading'>
          Feel the music
        </h1>
        <span class='content_text'>
          Stream over 10 million songs with one click
        </span>

        ${Button('Join now')}
      
      </section>
    </main>
  `
}