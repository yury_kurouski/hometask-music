import { Button } from '../../components/Button'
import './styles.css'

export const DiscoverPage = () => {
  return `
    <main class='container discover'>
      <section class='discover_content'>
        <h1 class='discover_heading'>
          Discover new music
        </h1>
        <div class='discover_buttons'>
          ${Button('Charts')}
          ${Button('Songs')}
          ${Button('Artists')}
        </div>
        <span class='discover_text'>
          By joing you can benefit by listening to the latest albums released
        </span>
      </section>
    </main>

  `
}