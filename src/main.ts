import {  renderPage } from './router';
import './styles/style.css'

window.addEventListener('load', renderPage);
window.addEventListener('popstate', renderPage);
